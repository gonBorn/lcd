import java.util.Arrays;
import java.util.Scanner;

public class LCD {
    private static int[][][] originModel =
            {
                    {{0,1,0}, {2,0,2}, {0,0,0}, {2,0,2}, {0,1,0}},
                    {{0,0,0}, {0,0,2}, {0,0,0}, {0,0,2}, {0,0,0}},
                    {{0,1,0}, {0,0,2}, {0,1,0}, {2,0,0}, {0,1,0}},
                    {{0,1,0}, {0,0,2}, {0,1,0}, {0,0,2}, {0,1,0}},
                    {{0,0,0}, {2,0,2}, {0,1,0}, {0,0,2}, {0,0,0}},
                    {{0,1,0}, {2,0,0}, {0,1,0}, {0,0,2}, {0,1,0}},
                    {{0,1,0}, {2,0,0}, {0,1,0}, {2,0,2}, {0,1,0}},
                    {{0,1,0}, {0,0,2}, {0,0,0}, {0,0,2}, {0,0,0}},
                    {{0,1,0}, {2,0,2}, {0,1,0}, {2,0,2}, {0,1,0}},
                    {{0,1,0}, {2,0,2}, {0,1,0}, {0,0,2}, {0,1,0}}
            };
    private static int [][][] amplifyModel;
    //deliver if only received half money by client
    public static void printSingleLCD(int[][] para) {
        for (int i = 0 ;i < 5;i++){
            for (int j = 0; j < 3 ;j++) {
                numToChar(para, i, j);
                if (j == 2) {
                    System.out.print("\r\n");
                }
            }
        }
    }

    public static void printLCD(int[][] lcdView) {
        int length = lcdView[0].length;
        for (int i = 0 ;i < lcdView.length;i++){
            for (int j = 0; j < length;j++) {
                numToChar(lcdView, i, j);
                if (j == length-1) {
                    System.out.print("\r\n");
                }
            }
        }
    }


    private static void numToChar(int lcdView[][], int i, int j) {
        switch (lcdView[i][j]) {
            case 0: System.out.print(" ");break;
            case 1: System.out.print("_");break;
            case 2: System.out.print("|");break;
        }
    }

    private static int[][] generateView(int[] paraArray, int[][][] model,int amplifyNum) {
        int paraNum = paraArray.length;
        int[][] lcdView =new int[3+2*amplifyNum][3*amplifyNum*paraNum];
        for (int p=0;p<paraNum;p++) {
            int paramater = paraArray[p];
            for (int i=0;i<3+2*amplifyNum;i++) {
                for (int j=0;j<3*amplifyNum;j++){
                    lcdView[i][j+p*3*amplifyNum] = model[paramater][i][j];
                }
            }
        }
        return lcdView;
    }

    public static int[][][] amplify(int amplifyNum) {
        int[][][] amplifyModel = new int[10][3+2*amplifyNum][3*amplifyNum];
        for (int p=0;p<10;p++) {
            //偶数行为下划线横向放大
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++){
                    amplifyModel[p][i+amplifyNum*i][j*2] = originModel[p][i*2][j];
                    amplifyModel[p][i+amplifyNum*i][j*2+1] = originModel[p][i*2][j];
                }
            }
            //奇数行为|，加大行数
            for (int i=1;i<5;i+=2) {
                for (int j=0;j<3;j++){
                    switch (i){
                        case 1:
                            for (int k=0;i<amplifyNum;i++){
                                amplifyModel[p][i+k][j*amplifyNum] = originModel[p][i][j];
                            }
                        case 3:
                            for (int k=0;i<amplifyNum;i++){
                                amplifyModel[p][amplifyNum+1+k][j*amplifyNum] = originModel[p][i][j];
                            }
                    }
//                    amplifyModel[p][i+amplifyNum*][j*2] = originModel[p][i][j];
//                    amplifyModel[p][i][j*2+1] = originModel[p][i][j];
//                    amplifyModel[p][i+1] = Arrays.copyOf(amplifyModel[p][i], amplifyModel[p][i].length);
                }
            }
        }
        return amplifyModel;

    }

    public static void main(String[] args) {
        System.out.println("请输入LCD数字串：");
        Scanner numStr = new Scanner(System.in);
        String[] paramaterStr = numStr.nextLine().split("");
        int[] paraArray = new int[paramaterStr.length];
        for (int i =0;i<paramaterStr.length;i++) {
            paraArray[i] = Integer.parseInt(paramaterStr[i]);
        }

        System.out.println("请输入放大倍数：");
        Scanner factor = new Scanner(System.in);
        int amplifyNum = factor.nextInt();

        factor.close();
        numStr.close();

        if (amplifyNum ==1) {
            printLCD(generateView(paraArray, originModel, 1));
        }
        else {
            amplifyModel = amplify(amplifyNum);
            printLCD(generateView(paraArray, amplifyModel, amplifyNum));
        }
    }

}
